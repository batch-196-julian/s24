/*
    Activity:

    1. Update and Debug the following codes to ES6.
        Use template literals,
        Use array/object destructuring,
        Use arrow functions
    
    2. Create a class constructor able to receive 3 arguments
        -It should be able to receive two strings and a number
        -Using the this keyword assign properties:
            name, 
            breed, 
            dogAge = <7x human years> 
            -assign the parameters as values to each property.



    Create 2 new objects using our class constructor.

    This constructor should be able to create Dog objects.

    Log the 2 new Dog objects in the console.


    Pushing Instructions:

    Create a git repository named S24.
    Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
    Add the link in Boodle.

*/

//Solution: 

/*Debug*/
let student1 = {
    name: "Shawn Michaels",
    birthday: "May 5, 2003",
    age: 18,
    isEnrolled: true,
    classes: ["Philosphy 101" ,"Social Sciences 201"]
}


let student2 = {
    name: "Steve Austin",
    birthday: "June 15, 2001",
    age: 20,
    isEnrolled: true,
    classes: ["Philosphy 401", "Natural Sciences 402"]
}


/*Debug and Update*/

const introduce = (student) =>{

    console.log(`Hi I'm ${student.name}. I am ${student.age} years old `)
    // console.log("Hi! I'm " + student.name + "." + " I am " + student.age + " years old.")
    console.log(`I study the following courses: ${student.classes}`)
    // console.log("I study the following courses: " + student.classes)

}

introduce(student1);
introduce(student2);




let getCube = (num) => num ** num

    
// update to arrow 
let numArr = [15,16,32,21,21,2] 

numArr.forEach(function(num){
    console.log(num);
})

let numsSquared = numArr.map(function(num){

    return num ** 2;
  }
)


console.log(numsSquared);


/*2. Class Constructor*/



class Dog{
    constructor(name,breed,dogAge){

        this.Name = name;
        this.Breed = breed;
        this.Dog_Age = dogAge *80;

    }


}
let dog1 = new Dog("Browny", "Pitbull", 10);
let dog2 = new Dog("Blacky", "Askal", 8);

console.log(dog1);
console.log(dog2);

class Team{

    constructor(username,role,guildName,level){

        this.Username = username;
        this.Role = role;
        this.Guild_Name = guildName;
        this.Level = level


    }
}
let team1 = new Team("josh003","Support", "Black Ninja", 10);
let team2 = new Team("blackmamba22","Tank", "Green Oracle", 12);

console.log(team1);
console.log(team2);



