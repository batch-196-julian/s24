// console.log("hello")

let fivePowerOf3 = Math.pow(5,3);
console.log(fivePowerOf3);


let fivePowerOf2 = 5**2;
console.log(fivePowerOf2);

let fivePowerOf4 = 5**4;
console.log(fivePowerOf4);

let squareRootOf4 = 4**.5;
console.log(squareRootOf4);

let string1 = "Javascript";
let string2 = "not";
let string3 = "is";
let string4 = "Typescript";
let string5 = "Java";
let string6 = "Zuitt";
let string7 = "Coding";

// let sentence1 = (string1 +" "+ string3 + " " + string2 + " " + string5) ;

// let sentence2 = (string4 +" "+ string3 + " " + string1) ;

// console.log(sentence1);
// console.log(sentence2);

// Template Literals
// "", '' - string literals

let sentence1 = `${string1} ${string3} ${string2} ${string5}` ;
let sentence2 = `${string4} ${string3} ${string1}`;
 
console.log(sentence1);
console.log(sentence2);

let sentence3 = `${string6} ${string7} Bootcamp.`;
console.log(sentence3);

let sentence4 = `The sum of 15 and 25 is ${15+25}.`;
console.log(sentence4);

let person = {

	name: "Michael",
	position: "developer",
	income: 50000,
	expenses: 60000

}

console.log(`${person.name} is a ${person.position}.`)
console.log(`His income is ${person.income} and expenses at ${person.expenses}. His current balance is ${person.income - person.expenses}.`)

let array1 = ["Curry", "Lillard", "Paul", "Irving"];

console.log(`${array1[0]}`)
let [player1,player2,player3,player4] = array1;

console.log(player1,player2,player3,player4);

let pokemon1 = {

    name: "Bulbasaur",
    type: "Grass",
    level: 10,
    moves: ["Razor Leaf", "Tackle", "Leech Seed"]

};

console.log(pokemon1);
let {level, type, name,moves,personality} = pokemon1;

console.log(level);
console.log(type);
console.log(name);
console.log(moves);
console.log(personality);
let pokemon2 = {

    name: "Charmander",
    type: "Grass",
    level: 10,
    moves: ["Razor Leaf", "Tackle", "Leech Seed"]

}

const {name: name2} = pokemon2;
console.log(name2);

// Arrow Functions are alternative way of writing function i JS.
// Traditional function
function diplayMsg(){
	console.log("Hello World");
}

diplayMsg();

// Arrow function

const hello = () => {

	console.log('Hello From Arrow!');

}
hello();

// Arrow with parrameter

const greet = (friend) => {

	// console.log(friend);
	console.log (`Hi ${friend.name}`);
}
greet(pokemon2);

// Arrow vs Traditional function

// function addNum(num1,num2){

// 	// console.log(num1,num2);
// 	// let result = num1+num2
// 	return num1+num2;

// }
// let sum = addNum(5,10);
// console.log(sum);

let subNum = (num1,num2) => num1 - num2;

let difference = subNum(10,5);

console.log(difference);



let addNum = (num1,num2) => num1 + num2

let sum = addNum (50,70);
console.log(sum);

// Traditional functions vs Arrow function as Methods

let character1 = {

	name: "Cloud Strife",
	occupation: "SOLDIER",
	greet: function(){
			// console.log(this);
			console.log(`Hi I'm ${this.name}.`);

	},

	introduceJob: () => {

	}
}

character1.greet();
character1.introduceJob();

// Class Based Objects Blueprints
// Constructor Function
function Pokemon(name,type,level){

	this.name = name;


}

class Car {

	constructor( brand,name,year){

		this.brand = brand;
		this.name = name;
		this.year = year;


	}
}

let car1 = new Car ("Toyota", "Vios", 2002);
let car2 = new Car ("Cooper", "Mini", 1969);
let car3 = new Car ("Porsche", "911", 1967);

console.log(car1);
console.log(car2);
console.log(car3);



class Pokemons {
	constructor(name,type,level,moves) {

		this.name = name;
		this.type = type;
		this.level = level;
		this.moves = moves;


	}

}
let pokemon01 = new Pokemons("Bulbasaur","Grass", 10,['Tackle', "target"]);
let pokemon02 = new Pokemons("Pickachu","Electric", 100,['Tackle', "target"]);

console.log(pokemon01);
console.log(pokemon02);